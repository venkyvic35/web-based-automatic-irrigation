#!/usr/bin/python
import serial, time
import requests
ser = serial.Serial('/dev/ttyACM0',  9600, timeout = 0.1)
# arduino and raspeberry pi communicated using serial port ttyACM0 
time.sleep(7)
k=0
#if you only want to send data to arduino (i.e. a signal to move a servo)
def send( theinput ):
  ser.write( theinput )
  while True:
    try:
      time.sleep(0.05)
      break
    except:
      pass
  time.sleep(0.5)

#if you would like to tell the arduino that you would like to receive data from the arduino
def send_and_receive( theinput ):
  ser.write( theinput )
  while True:
    try:
      time.sleep(0.05)
      state = ser.readline()
      print state
      return state
    except:
      pass
  time.sleep(0.5)

f = open('dataFile.txt','a')

while 1 :
    # first reading useroption from web..if option=1 automatic working ..2 turn off pump..3 turn on pump
    url1 = 'http://192.168.43.99:808/onlineshop/donor/options.php' 
    query1 = {'hello': 17}
    res1 = requests.post(url1, data=query1)
    k=res1.text
    print(res1.text)
    
    if(res1.text=='1'):
     arduino_sensor = send_and_receive('1')
     url = 'http://192.168.43.99:808/onlineshop/donor/insertsensordata.php'
     query = {'senvalue': arduino_sensor}
     res = requests.post(url, data=query)
     print(res.text)
     '''time.sleep(5)'''
     '''f.write(arduino_sensor)'''
     f.close()
     f = open('dataFile.txt','a')
    elif(res1.text=='2'):
       print('pump turn on')
       arduino_sensor = send('2')
       time.sleep(0.5)
    else :
       print('pump turn off')
       arduino_sensor = send('3')
       time.sleep(0.5)